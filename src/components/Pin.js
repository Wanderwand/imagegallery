import React from "react";
import "./Pin.css";

function Pin(props) {
  let { urls } = props;

  return (
    <div className="pin" onClick={props.onClick} onMouseOut={props.onMouseOut}>
      <div className="pin__container">
       <img src={urls?.regular} alt="pin" className="img" title="Click on Image" /> 
      </div>
    </div>

  );
}

export default Pin;

