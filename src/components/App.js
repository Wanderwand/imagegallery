import "./App.css";
import { useState, useRef, useEffect } from "react";
import Header from "./Header";
import Mainboard from "./Mainboard";
import unsplash from "../api/unsplash";
import { AirlineSeatFlatSharp } from "@material-ui/icons";
import CircularProgress from "@material-ui/core/CircularProgress";

function App() {
  const [newPins, setNewPins] = useState([]);
  const [defaultPins, setDefaultPins] = useState([]);
  const [pageNo, setPageNo] = useState(0);
  const [flag, setFlag] = useState(false);
  const [loading, setLoading] = useState(false);

  const loader = useRef(null);

  const getImages = (term , no = 10 ) => {
    return unsplash.get("https://api.unsplash.com/search/photos", {
      params: { query: term, page: pageNo  , per_page : no },
    });
  };

  const onSearchSubmit = (term) => {
    getImages(term ).then((res) => {
      let results = res.data.results;
      let pins = [...results];
      // pins.sort(function (a, b) {
      //   return 0.5 - Math.random();
      // });
      setNewPins([...newPins, ...pins]);
      console.log(newPins);
    });
  };

  const resetImput = () => {
    setNewPins([]);
  };

  // useEffect(() => {
  //   let pinData = [];

  //   let pins = ["books", "cars", "dogs", "elephants"];
  //   const prom = async () => {
  //     for (let pinTerm of pins) {
  //       const res = await getImages(pinTerm);
  //       pinData = pinData.concat(res.data.results);
  //       console.log(res.data.results);
  //       console.log(pinData);
  //     }
  //     return pinData;
  //   };
  //   prom().then((arr) => {
  //     setDefaultPins(arr);
  //     // console.log(defaultPins)
  //   });
  // }, []);

  //////   loader
  useEffect(() => {
    var options = {
      root: null,
      rootMargin: "20px",
      threshold: 1.0,
    };
    const observer = new IntersectionObserver(handleObserver, options);
    if (loader.current) {
      observer.observe(loader.current);
    }
  }, []);

  // useEffect(() => {
  //   console.log(pageNo)
  // }, [pageNo]);

  const handleObserver = (entities) => {
    const target = entities[0];
    if (target.isIntersecting) {
      setPageNo((pre) => pre + 1);
      console.log(pageNo);
      setFlag(true);
      setLoading(true);
    } else {
      setFlag(false);
      setLoading(false);
    }
  };

  useEffect(() => {
    console.log(loading);
  }, [loading]);
  return (
    <div className="app">
      <Header onSubmit={onSearchSubmit} flag={flag} resetInput={resetImput} />
      <Mainboard pins={newPins} />
      {/* <Mainboard pins={defaultPins} /> */}
      {/* {!newPins ? <Mainboard pins={defaultPins} /> : <Mainboard pins={newPins} />} */}

      <div ref={loader}>
        {newPins.length === 0 ? (
          <div style={{ textAlign: "center", margin: "20px" }}>
            <h2>Enter correct keywords ....</h2>
          </div>
        ) : loading ? (
          <div style={{ textAlign: "center" , margin : "20px" }}>
            <CircularProgress />
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default App;
