import React,{useState} from "react";
import Pin from "./Pin";
import "./Mainboard.css";

function Mainboard(props) {
  let { pins } = props;
  const [active, setActive] = useState(false);
  const [activeImg, setActiveImg] = useState("");

  const onClick = (e) => {
    if(e.target.src){
      setActive(true);
      setActiveImg(e.target.src);

    }
  };
  const onMouseOut = (e) => {
    setActive(false);
    setActiveImg("");
  };
  
  return (
    <div className="mainboard">
      <div className="mainboard__container">
      { pins.map((pin) => {
          let { urls } = pin;
          return <Pin urls={urls} onClick={onClick} />;
        })}
      </div>
      {active? 
      (<div  className="modal" onClick={onMouseOut}>
        <img src={activeImg} className="active__img"  alt="pin2" />
        
      </div>) : null 
      }
   
    </div>
  );
}

export default Mainboard;
