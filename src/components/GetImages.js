import React, { useEffect, useState } from "react";
import unsplash from "../api/unsplash";
import axios from "axios";

export default function GetImages(term , page_no) {
  const [pageNo  , setPageNo ] = useState(page_no )
  const [loading , setLoading ] = useState(false)
  const [images , setImages ] = useState([])


  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();

  useEffect(()=>{
    setLoading(true)
    unsplash.get("https://api.unsplash.com/search/photos", {
      params: { query: term, page_no: pageNo },
      cancelToken : source.token
    }).then(res=>{
      setImages([...res.data.result])
      setPageNo(pageNo+1)
      setLoading(true)

    }).catch(err=>{
      if(axios.isCancel(err)){
        console.log("error : " , err)
      }else{
        console.log(err)
      }
    })

    return ()=>source.cancel("Request canceled !")
  },[])

  return {loading  , images , pageNo}
}

